<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

  //cek username & password Petugas
  function auth_petugas($username,$password) {
    $query = $this->db->query("SELECT * FROM petugas WHERE username = '$username' AND password = sha1('$password')");
    return $query;
  }

  function get_level() {
    return $this->db->query('SELECT * FROM level ORDER BY nama_level ASC');
  }

  function get_edit($post) {
    $this->db->select("id_petugas, nama_petugas, username, password, id_level");
    $this->db->from("petugas");
    $this->db->where_in('id_petugas', $post['id_petugas']);
    $this->db->order_by('id_petugas', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('petugas', $result, 'id_petugas');
    }
  }

}