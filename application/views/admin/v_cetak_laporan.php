<!DOCTYPE html>
<html>
<head>
  <title>Cetak PDF</title>
  <style type="text/css">
    h3,
    p {
      line-height: 10px;
      margin-bottom: 0px;
    }
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
    }
    th {
      background: #20a8d8;
    }
    th,
    td {
      padding: 5px;
      text-align: center;
    }
  </style>
</head>
<body>
  <div style="display: block;">
    <div style="float: left; position: absolute;">
      <img style="width: 200px ; height: 150px; margin-top: 40px; margin-left: 5px;" src="<?php echo base_url()."assets/img/bogor.png" ?>">
    </div>
    <div style="text-align: center;">
      <h3>PEMERINTAH KABUPATEN BOGOR</h3>
      <h3>DINAS PENDIDIKAN</h3>
      <h3>SMK NEGERI 1 CIOMAS</h3>
      <P>TEKNOLOGI DAN REKAYASA</P>
      <P>TEKNOLOGI INFORMASI DAN KOMUNIKASI</P>
      <P>NSS : 401020229101 NSPN : 20254135</P>
      <P>JL. Raya Laladon Des. Laladon Kec. Ciomas Kab. Bogor Telp : (0251) 8631216 Kode Pos. 16610</P>
    </div>  
    <div style="float: right; position: absolute; right: 0;">
      <img style="width: 100px ; height: 120px; margin-top: 50px; margin-right: 50px;" src="<?php echo base_url()."assets/img/ciomas.png" ?>">
    </div>
  </div>
  
  <hr style="margin-top: 20px;">
  <div style="text-align: center; margin-bottom: 20px;">
    <h3>Data Barang</h3>
    <p style=""><?php echo $ket; ?></p>
  </div>

  <table width="100%" border="1px;" style="border-collapse: collapse;" align="center">
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Spesifikasi</th>
        <th>Lokasi</th>
        <th>Jumlah</th>
        <th>Kondisi</th>
        <th>Jenis</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <?php
      if(!empty($inventaris)) {
        $no = 1;
        foreach($inventaris as $v){
          $tgl = date('d-m-Y', strtotime($v->tanggal_register));                      
          echo "<tr>";
          echo "<td>".$no."</td>";
          echo "<td>".$v->kode_inventaris."</td>";
          echo "<td>".$v->nama."</td>";
          echo "<td>".$v->spesifikasi."</td>";
          echo "<td>".$v->nama_ruang."</td>";
          echo "<td>".$v->jumlah."</td>";
          echo "<td>".$v->kondisi."</td>";
          echo "<td>".$v->nama_jenis."</td>";
          if ($v->ket == 'Y') {
            echo "<td>Tersedia</td>";
          } else {
            echo "<td>Tidak Tersedia</td>";
          }      
          echo "</tr>";
          $no++;
        }
      }
    ?>
  </table>

  <div style="display: block; margin-top: 40px;">
    <div style="float: right; text-align: right;">
      <p style="margin-bottom: 40px; margin-right: 50px;">Bogor, <?php echo date('d M y'); ?></p>    
      <p style="margin-right: 60px;"><?php echo $this->session->userdata('ses_name'); ?></p>
    </div>
  </div>

</body>
</html>