<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Jenis Barang</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li class="active">
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item">Jenis Barang</li>
      <li class="breadcrumb-item active">Tambah </li>
    </ol>

    <div class="card">
      <div class="card-header">
        Tambah Jenis Barang
        <div class="float-right">
          <form method="GET" action="<?php echo base_url().'Crud_jenis_barang/tambah' ?>">
            <div class="input-group">
              <label>Form &nbsp;</label>
              <input type="text" name="total_form" value="1" class="form-control form-control-sm"
              onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
              onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
              <button type="submit" name="submit" value="ok" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <?=$this->session->flashdata('notif');?>
        <form method="post" action="<?php echo base_url().'Crud_jenis_barang/tambah_proses' ?>">
          <div class="table-responsive pt-2">
            <table class="table table-bordered" id="">
              <thead class="bg-core">
                <tr class="text-center">
                  <th>No</th>
                  <th>Kode Jenis</th>
                  <th>Nama Jenis</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $this->load->helper('string');
                  $no = 1;
                  $kode = random_string('alnum', 4);
                  for ($i=0; $i < $total_form; $i++) { 
                    echo "<tr>
                            <td>".$no++."</td>
                            <td><input type='text' name='kode_jenis[]' value='JNS-$kode$no' class='form-control' readonly></td>
                            <td><input type='text' name='nama_jenis[]' class='form-control' required='' autocomplete='off' onkeypress='hurufangka(event)'></td>
                            <td>
                              <select name='keterangan[]' class='form-control' required=''>
                                <option selected='' disabled=''>--Pilih--</option>
                                <option value='Y'>Tersedia</option>
                                <option value='N'>Tidak Tersedia</option>
                              </select>
                            </td>
                          </tr>";       
                  }
                ?>
              </tbody>
            </table>
            <a href="<?php echo base_url().'Crud_jenis_barang/' ?>"class="btn btn-primary"><i class="fa fa-arrow-alt-circle-left"></i> Batal</a>
            <button type="submit" name="simpan" value="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>

</body>
</html>