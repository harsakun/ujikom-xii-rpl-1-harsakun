  <script>
    var baseurl = "<?php echo base_url(""); ?>"; // Buat variabel baseurl untuk nanti di akses pada file config.js
  </script>
  <script src="<?php echo base_url().'assets/vendor/jquery/jquery.min.js' ?>"></script>
  <script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
  <script src="<?php echo base_url().'assets/vendor/DataTables/datatables.js' ?>"></script>
  <script src="<?php echo base_url().'assets/js/jquery.mCustomScrollbar.concat.min.js' ?>"></script>
  <script src="<?php echo base_url().'assets/js/config.js' ?>"></script>
  <script src="<?php echo base_url().'assets/js/input_detect.js' ?>"></script>

  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    });
    $(function () {
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    });

    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });

    $(document).ready(function () {
      // When the user scrolls the page, execute myFunction 
      window.onscroll = function() {myFunction()};

      // Get the header
      var header = document.getElementById("myHeader");

      // Get the offset position of the navbar
      var sticky = header.offsetTop;

      // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("sticky-top");
        } else {
          header.classList.remove("sticky-top");
        }
      }
    });
  </script>

  <script type="text/javascript">
    function inputDigitsOnly(e) {
     var chrTyped, chrCode=0, evt=e?e:event;
     if (evt.charCode!=null)     chrCode = evt.charCode;
     else if (evt.which!=null)   chrCode = evt.which;
     else if (evt.keyCode!=null) chrCode = evt.keyCode;

     if (chrCode==0) chrTyped = 'SPECIAL KEY';
     else chrTyped = String.fromCharCode(chrCode);

     //[test only:] display chrTyped on the status bar 
     self.status='inputDigitsOnly: chrTyped = '+chrTyped;

     //Digits, special keys & backspace [\b] work as usual:
     if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
     if (evt.altKey || evt.ctrlKey || chrCode<28) return true;

     //Any other input? Prevent the default response:
     if (evt.preventDefault) evt.preventDefault();
     evt.returnValue=false;
     return false;
    }

    function addEventHandler(elem,eventType,handler) {
     if (elem.addEventListener) elem.addEventListener (eventType,handler,false);
     else if (elem.attachEvent) elem.attachEvent ('on'+eventType,handler); 
     else return 0;
     return 1;
    }

    // onload: Call the init() function to add event handlers!
    function init() {
     addEventHandler(self.document.f2.elements[0],'keypress',inputDigitsOnly);
     addEventHandler(self.document.f2.elements[1],'keypress',inputDigitsOnly);
    }
  </script>