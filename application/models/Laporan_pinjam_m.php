<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_pinjam_m extends CI_Model {

  function view_by_date($date) {
    // $this->db->where('DATE(tanggal_register)', $date); // Tambahkan where tanggal nya
        
    return $this->db->query("SELECT * FROM detail_pinjam d INNER JOIN peminjaman p ON d.id_detail_pinjam=p.id_detail_pinjam JOIN inventaris i ON d.id_inventaris=i.id_inventaris JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN petugas t ON d.id_petugas=t.id_petugas WHERE DATE(tanggal_pinjam)='$date' ORDER BY p.id_peminjaman DESC")->result(); // Tampilkan data inventaris sesuai tanggal yang diinput oleh user pada filter
  }

  function view_by_month($month, $year) {
    // $this->db->where('MONTH(tanggal_register)', $month); // Tambahkan where bulan
    // $this->db->where('YEAR(tanggal_register)', $year); // Tambahkan where tahun
        
    return $this->db->query("SELECT * FROM detail_pinjam d INNER JOIN peminjaman p ON d.id_detail_pinjam=p.id_detail_pinjam JOIN inventaris i ON d.id_inventaris=i.id_inventaris JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN petugas t ON d.id_petugas=t.id_petugas WHERE MONTH(tanggal_pinjam)='$month' AND YEAR(tanggal_pinjam)='$year' ORDER BY p.id_peminjaman DESC")->result(); // Tampilkan data inventaris sesuai bulan dan tahun yang diinput oleh user pada filter
  }
    
  function view_by_year($year) {
    // $this->db->where('YEAR(tanggal_register)', $year); // Tambahkan where tahun
        
    return $this->db->query("SELECT * FROM detail_pinjam d INNER JOIN peminjaman p ON d.id_detail_pinjam=p.id_detail_pinjam JOIN inventaris i ON d.id_inventaris=i.id_inventaris JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN petugas t ON d.id_petugas=t.id_petugas WHERE YEAR(tanggal_pinjam)='$year' ORDER BY p.id_peminjaman DESC")->result(); // Tampilkan data inventaris sesuai tahun yang diinput oleh user pada filter
  }
    
  function view_all() {
    return $this->db->query("SELECT * FROM detail_pinjam d INNER JOIN peminjaman p ON d.id_detail_pinjam=p.id_detail_pinjam JOIN inventaris i ON d.id_inventaris=i.id_inventaris JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN petugas t ON d.id_petugas=t.id_petugas ORDER BY p.id_peminjaman DESC")->result(); // Tampilkan semua data inventaris
  }
    
  function option_tahun() {
    $this->db->select('YEAR(tanggal_pinjam) AS tahun'); // Ambil Tahun dari field tanggal_register
    $this->db->from('peminjaman'); // select ke tabel inventaris
    $this->db->order_by('YEAR(tanggal_pinjam)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
    $this->db->group_by('YEAR(tanggal_pinjam)'); // Group berdasarkan tahun pada field tanggal_register
    
    return $this->db->get()->result(); // Ambil data pada tabel inventaris sesuai kondisi diatas
  }

}