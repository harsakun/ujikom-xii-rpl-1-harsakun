<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Renime">

  <title>CInventoria | Login</title>

  <!-- Logo Favicon -->
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url().'assets/img/logo_inventaris.png' ?>">

  <!-- Load CSS login simple -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/login.css' ?>">
  <!-- Load CSS Bootstrap -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.css' ?>">
  <!-- Load Animasi CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/animate.min.css' ?>">
  <!-- Load Fontawesome icon -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/fontawesome/css/all.css' ?>">

</head>
<body style="background: #ec292a;">

  <div class="loginBox animated fadeIn">
    <img class="user" src="<?php echo base_url().'assets/img/user.png' ?>">
    <h3>Sign in here</h3>

    <?php echo $this->session->flashdata('msg') ?>

    <form action="<?php echo base_url().'Login/auth' ?>" method="post">
      <div class="inputBox">
        <input type="text" name="username" placeholder="Username" autocomplete="off">
        <span><i class="fa fa-user" aria-hidden="true"></i></span>
      </div>
      <div class="inputBox">
        <input type="password" name="password" placeholder="Password">
        <span><i class="fa fa-lock" aria-hidden="true"></i></span>
      </div>
      <input type="submit" name="" value="login">
    </form>
    <div class="text-center">
      <a href="https://gitlab.com/harsakun/ujikom-xii-rpl-1-harsakun" target="_blank" style="text-decoration: none;">Copyright <i class="fa fa-copyright"></i>. CInventoria</a>  
    </div>    
  </div>

  <!-- Load jquery-slim -->
  <script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.slim.min.js' ?>"></script>
  <!-- Load JS Bootstrao -->
  <script type="text/javascript" src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.js' ?>"></script>

</body>
</html>