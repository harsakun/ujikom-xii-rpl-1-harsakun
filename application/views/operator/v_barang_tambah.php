<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Barang</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('operator/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li class="active">
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('operator/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('operator/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item">Barang</li>
      <li class="breadcrumb-item active">Tambah </li>
    </ol>

    <div class="card">
      <div class="card-header">
        Tambah Barang
        <div class="float-right">
          <form method="GET" action="<?php echo base_url().'Crud_barang/tambah' ?>">
            <div class="input-group">
              <label>Form &nbsp;</label>
              <input type="text" name="total_form" value="1" class="form-control form-control-sm"
              onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
              onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
              <button type="submit" name="submit" value="ok" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <?=$this->session->flashdata('notif');?>
        <form method="post" action="<?php echo base_url().'Crud_barang/tambah_proses' ?>">
          <?php 
            $this->load->helper('string');
            date_default_timezone_set('asia/jakarta');
            $tgl = date('Y-M-d D');
            $petugas = $this->session->userdata('ses_id');
            $no = 1;
            $kode = random_string('alnum', 4);
            for ($i=0; $i < $total_form; $i++) { 
          ?>
            <span class="badge badge-primary">Data Ke - <?php echo $no++; ?></span>
            <div class="float-right">
              <input type="text" name="tanggal_register[]" value="<?php echo $tgl; ?>" class="form-control" readonly=''>
            </div>
            <hr>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label>Kode Barang</label>
                <input type='text' name='kode_inventaris[]' value='<?php echo "BRG-$kode$no"; ?>' class='form-control' required='' readonly=''>
                <small class="form-text text-muted">* Kode sudah terisi secara otomatis</small>
              </div>
              <div class="form-group col-md-6">
                <label>Nama Barang</label>
                <input type='text' name='nama[]' class='form-control' required='' autocomplete='off' onkeypress="hurufangka(event)">
              </div>
              <div class="form-group col-md-3">
                <label>Jumlah</label>
                <input type='text' name='jumlah[]' class='form-control' required='' autocomplete='off'
                onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
              </div>
            </div>
            <div class="form-group">
              <label>Spesifikasi</label>
              <textarea name='spesifikasi[]' rows="3" class='form-control' required='' onkeypress="hurufangka(event)"></textarea>
            </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label>Kondisi</label>
                <select name='kondisi[]' class='form-control' required=''>
                  <option selected='' disabled=''>--Pilih--</option>
                  <option value='Baik'>Baik</option>
                  <option value='Rusak'>Rusak</option>
                  <option value='Rusak Parah'>Rusak Parah</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Jenis</label>
                <select name="id_jenis[]" class='form-control'>
                  <option selected="" disabled="">--Pilih--</option>
                  <?php foreach ($data_jenis as $v) { ?>
                    <option value="<?php echo $v->id_jenis; ?>"><?php echo $v->nama_jenis; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Tempat</label>
                <select name="id_ruang[]" class='form-control'>
                  <option selected="" disabled="">--Pilih--</option>
                  <?php foreach ($data_ruang as $v) { ?>
                    <option value="<?php echo $v->id_ruang; ?>"><?php echo $v->nama_ruang; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Keterangan</label>
                <select name='keterangan[]' class='form-control' required=''>
                  <option selected='' disabled=''>--Pilih--</option>
                  <option value='Y'>Tersedia</option>
                  <option value='N'>Tidak Tersedia</option>
                </select>
              </div>
              <input type="hidden" name="id_petugas[]" value="<?php echo $petugas; ?>">
            </div>
            <hr>
            <?php            
              }
            ?>        
            
            <a href="<?php echo base_url().'Crud_barang/' ?>"class="btn btn-primary"><i class="fa fa-arrow-alt-circle-left"></i> Batal</a>
            <button type="submit" name="simpan" value="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>
  
  <!-- Load link js -->
  <?php $this->load->view('operator/link-js-footer'); ?>

</body>
</html>