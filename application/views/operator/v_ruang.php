<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Tempat / Ruangan</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('operator/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li class="active">
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('operator/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('operator/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item active">Tempat / Ruangan</li>
    </ol>

    <div class="bd-callout bd-callout-warning">
      <h4 id="third-party-libraries">Catatan :</h4>
      <p><strong>* Harap centang terlebih dahulu jika ingin mengubah data.</strong><br><strong>* Silahkan ubah angka pada form tambah di pojok kanan dengan angka yang kamu butuhkan.</strong></p>
    </div>

    <div class="row">
      
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">
            Jumlah Data
          </div>
          <div class="card-body">
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><i class="fa fa-home"></i> Ruangan 
                <span class="badge badge-primary float-right"><?php echo $this->Crud_ruang_m->get_jml_ruang(); ?></span>
              </li>
              <li class="list-group-item"><i class="fa fa-check"></i> Tersedia
                <span class="badge badge-success float-right"><?php echo $this->Crud_ruang_m->get_jml_tersedia(); ?></span>
              </li>
              <li class="list-group-item"><i class="fa fa-"></i> Tidak Tersedia
                <span class="badge badge-warning float-right"><?php echo $this->Crud_ruang_m->get_jml_ttersedia(); ?></span>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-9">
        <div class="card">
          <div class="card-header">
            Data Tempat / Ruangan
            <div class="float-right">
              <form method="GET" action="<?php echo base_url().'Crud_ruang/tambah' ?>">
                <div class="input-group">
                  <label>Form &nbsp;</label>
                  <input type="text" name="total_form" value="1" class="form-control form-control-sm"
                  onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                  onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
                  <button type="submit" name="submit" value="ok" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button>
                </div>
              </form>
            </div>
          </div>
          <div class="card-body">
            <?=$this->session->flashdata('notif');?>
            <form method="post" action="<?php echo base_url().'Crud_ruang/sunting_hapus' ?>">
              <div class="table-responsive pt-2">
                <table class="table table-bordered" id="example1">
                  <thead class="bg-core">
                    <tr class="text-center">
                      <th width="10">#</th>
                      <th width="10">No</th>
                      <th>Kode Ruang</th>
                      <th>Nama Ruang</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if ($data_count == 0) {
                        echo "data kosong";
                      } else {
                        $no = 1;
                        foreach ($data as $v) {
                          echo "<tr>";
                          echo "<td class='text-center'><input type='checkbox' class='check-item' name='check[]' value='{$v->id_ruang}'></td>";
                          echo "<td>".$no++."</td>";
                          echo "<td>".$v->kode_ruang."</td>";
                          echo "<td>".$v->nama_ruang."</td>";
                          if ($v->keterangan == 'Y') {
                            echo "<td><span class='badge badge-primary'>Tersedia</span></td>";
                          } else {
                            echo "<td><span class='badge badge-warning'>Tidak Tersedia</span></td>";
                          }                          
                          echo "</tr>";
                        }
                      }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="4">Opsi</td>
                      <td>
                        <button type="submit" name="sunting" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i> Edit</button>
                        <button class="btn btn-danger btn-sm" disabled=""><i class="fa fa-trash"></i> Hapus</button>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
    
  </div>
</div>
  
  <!-- Load link js -->
  <?php $this->load->view('operator/link-js-footer'); ?>

</body>
</html>