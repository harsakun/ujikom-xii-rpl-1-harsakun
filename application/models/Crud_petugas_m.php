<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_petugas_m extends CI_Model {

  function get_level() {
    return $this->db->query('SELECT * FROM level ORDER BY nama_level ASC');
  }

  function get_jml_admin() {
    return $this->db->query("SELECT * FROM petugas WHERE id_level='1'")->num_rows();
  }

  function get_jml_operator() {
    return $this->db->query("SELECT * FROM petugas WHERE id_level='2'")->num_rows();
  }

  function get_jml_peminjam() {
    return $this->db->query("SELECT * FROM petugas WHERE id_level='3'")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM petugas pe INNER JOIN level le ON pe.id_level=le.id_level ORDER BY id_petugas DESC");
  }

  function get_edit($post) {
    $this->db->select("id_petugas, nama_petugas, username, id_level");
    $this->db->from("petugas");
    $this->db->where_in('id_petugas', $post['check']);
    $this->db->order_by('id_petugas', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_add($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->insert_batch('petugas', $result);
    }
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('petugas', $result, 'id_petugas');
    }
  }

  function post_delete($post = array()) {
    $total_array = count($post);

    if ($total_array != 0) {
      $this->db->where_in('id_petugas', $post['check']);
      $this->db->delete('petugas');
    }
  }

}