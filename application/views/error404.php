<!DOCTYPE html>
<html>
<head>
  
	<meta charset="utf-8">
	<meta name="viewport" content="width=ddevice-width, initial-scale=1.0">
	<meta name="author" content="Renime">

	<title>404 error not found</title>

  <!-- Logo Favicon -->
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url().'assets/img/inventaris.png' ?>">

	<!-- include CSS Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.css' ?>">
	<!-- include CSS Custom error 404 -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/error.css' ?>">

</head>
<body>

	<!-- Error Page -->
  <div class="error">
    <div class="container-floud">
      <div class="col-xs-12 ground-color text-center">
        <div class="container-error-404">
          <div class="clip"><div class="bayangan"><span class="digit thirdDigit"></span></div></div>
          <div class="clip"><div class="bayangan"><span class="digit secondDigit"></span></div></div>
          <div class="clip"><div class="bayangan"><span class="digit firstDigit"></span></div></div>
          <div class="msg">OH!<span class="triangle"></span></div>
        </div>
          <h2 class="h1">Sorry! Page not found</h2>
      </div>
    </div>
  </div>
  <!-- Error Page -->

	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-slim.min.js' ?>"></script>
	<script type="text/javascript">
		function randomNum()
    {
      "use strict";
      return Math.floor(Math.random() * 9)+1;
    }
      var loop1,loop2,loop3,time=30, i=0, number, selector3 = document.querySelector('.thirdDigit'), selector2 = document.querySelector('.secondDigit'),
          selector1 = document.querySelector('.firstDigit');
      loop3 = setInterval(function()
      {
        "use strict";
          if(i > 40)
          {
              clearInterval(loop3);
              selector3.textContent = 4;
          }else
          {
              selector3.textContent = randomNum();
              i++;
          }
      }, time);
      loop2 = setInterval(function()
      {
        "use strict";
          if(i > 80)
          {
              clearInterval(loop2);
              selector2.textContent = 0;
          }else
          {
              selector2.textContent = randomNum();
              i++;
          }
      }, time);
      loop1 = setInterval(function()
      {
        "use strict";
          if(i > 100)
          {
              clearInterval(loop1);
              selector1.textContent = 4;
          }else
          {
              selector1.textContent = randomNum();
              i++;
          }
      }, time);	
	</script>

</body>
</html>