<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Laporan_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') {
      
      if(isset($_GET['filter']) && !empty($_GET['filter'])) { // Cek apakah user telah memilih filter dan klik tombol tampilkan
        $filter = $_GET['filter']; // Ambil data filder yang dipilih user

        if($filter == '1') { // Jika filter nya 1 (per tanggal)

          if (empty($tgl = $_GET['tanggal'])) {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data tanggal tidak boleh kosong!</div>');
            redirect('Laporan');
          } else {
            $tgl = $_GET['tanggal'];
            $ket = 'Data Barang Tanggal '.date('d-m-y', strtotime($tgl));
            $url_cetak = 'Laporan/cetak?filter=1&tanggal='.$tgl;
            $inventaris = $this->Laporan_m->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di Laporan_m
          }

        } elseif($filter == '2') { // Jika filter nya 2 (per bulan)

          if (empty($bulan = $_GET['bulan']) && empty($tahun = $_GET['tahun'])) {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data Bulan dan Tahun tidak boleh kosong!</div>');
            redirect('Laporan');
          } elseif (empty($bulan = $_GET['bulan'])) {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data Bulan tidak boleh kosong!</div>');
            redirect('Laporan');
          } elseif (empty($tahun = $_GET['tahun'])) {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data Tahun tidak boleh kosong!</div>');
            redirect('Laporan');
          } else {
            $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

            $ket = 'Data Barang Bulan '.$nama_bulan[$bulan].' '.$tahun;
            $url_cetak = 'Laporan/cetak?filter=2&bulan='.$bulan.'&tahun='.$tahun;
            $inventaris = $this->Laporan_m->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di Laporan_m
          }

        } else { // Jika filter nya 3 (per tahun)

          if (empty($tahun = $_GET['tahun'])) {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data Tahun tidak boleh kosong!</div>');
            redirect('Laporan');
          } else {
            $ket = 'Data Barang Tahun '.$tahun;
            $url_cetak = 'Laporan/cetak?filter=3&tahun='.$tahun;
            $inventaris = $this->Laporan_m->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di Laporan_m
          }
          
        }
      } else { // Jika user tidak mengklik tombol tampilkan

        $ket = 'Semua Data Barang';
        $url_cetak = 'Laporan/cetak';
        $inventaris = $this->Laporan_m->view_all(); // Panggil fungsi view_all yang ada di Laporan_m

      }

      $data['ket'] = $ket;
      $data['url_cetak'] = base_url($url_cetak);
      $data['inventaris'] = $inventaris;
      $data['option_tahun'] = $this->Laporan_m->option_tahun();
      $this->load->view('admin/v_laporan_barang', $data);
      
    } elseif ($this->session->userdata('akses') == '2') {
      $this->load->view('error404');
    } elseif ($this->session->userdata('akses') == '3') {
      $this->load->view('error404');
    }

  }

  function cetak() {
    if (isset($_GET['filter']) && ! empty($_GET['filter'])) { // Cek apakah user telah memilih filter dan klik tombol tampilkan
      $filter = $_GET['filter']; // Ambil data filder yang dipilih user

      if ($filter == '1') { // Jika filter nya 1 (per tanggal)
        $tgl = $_GET['tanggal'];

        $ket = 'Data Barang Tanggal '.date('d-m-y', strtotime($tgl));
        $inventaris = $this->Laporan_m->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di Laporan_m
      } else if($filter == '2') { // Jika filter nya 2 (per bulan)
        $bulan = $_GET['bulan'];
        $tahun = $_GET['tahun'];
        $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

        $ket = 'Data Barang Bulan '.$nama_bulan[$bulan].' '.$tahun;
        $inventaris = $this->Laporan_m->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di Laporan_m
      } else { // Jika filter nya 3 (per tahun)
        $tahun = $_GET['tahun'];

        $ket = 'Data Barang Tahun '.$tahun;
        $inventaris = $this->Laporan_m->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di Laporan_m
      }
    } else { // Jika user tidak mengklik tombol tampilkan
      $ket = 'Semua Data Barang';
      $inventaris = $this->Laporan_m->view_all(); // Panggil fungsi view_all yang ada di Laporan_m
    }

    $data['ket'] = $ket;
    $data['inventaris'] = $inventaris;

    ob_start();
    $this->load->view('admin/v_cetak_laporan', $data);
    $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/vendor/html2pdf/html2pdf.class.php');
    $pdf = new HTML2PDF('L','A4','en');
    $pdf->WriteHTML($html);
    $pdf->Output('Data Barang.pdf', 'D');
  }

}