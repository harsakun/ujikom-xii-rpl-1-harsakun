<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Profile</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('peminjam/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-credit-card mr-2"></i> Peminjaman</a>
      </li>

    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('peminjam/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('peminjam/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item active">Profile </li>
    </ol>

    <div class="card">
      <div class="card-header">
        Edit Profile
      </div>
      <div class="card-body">
        <form method="post" action="<?php echo base_url().'Login/sunting_proses' ?>">
          <?php 
            $no = 1;
            foreach ($data as $key => $v) { 
          ?>
            <input type="hidden" name="id_petugas[]" value="<?php echo $v->id_petugas; ?>">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Username</label>
                <input type='text' name='username[]' value="<?php echo $v->username; ?>" class='form-control' required='' autocomplete='off' onkeypress="hurufangka_ts(event)">
              </div>
              <div class="form-group col-md-6">
                <label>Password</label>
                <input type='password' name='password[]' value="" class='form-control' autocomplete='off' onkeypress="hurufangka_ts(event)">
                <input type="checkbox" name="check[]">
                <small class="text-muted">* isi dan ceklis jika ingin mengubah password</small>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Nama</label>
                <input type='text' name='nama_petugas[]' value='<?php echo $v->nama_petugas; ?>' class='form-control' required='' onkeypress="hurufangka(event)">
              </div>
              <div class="form-group col-md-6">
                <label>Level</label>
                <input type='hidden' name='id_level[]' value='<?php echo $v->id_level; ?>' class='form-control' required=''>
                <select class='form-control' disabled="">
                  <option selected="" disabled="">--Pilih--</option>
                  <?php foreach ($data_level as $l) { ?>
                    <option value="<?php echo $v->id_level; ?>" <?php if ($l->id_level == $v->id_level) { echo "selected"; } ?> ><?php echo $l->nama_level; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <?php            
              }
            ?>        
            
            <a href="<?php echo base_url().'Page/' ?>"class="btn btn-primary"><i class="fa fa-arrow-alt-circle-left"></i> Batal</a>
            <button type="submit" name="simpan" value="simpan perubahan" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
            <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>
  
  <!-- Load link ls -->
  <?php $this->load->view('peminjam/link-js-footer'); ?>

</body>
</html>