<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_jenis_barang_m extends CI_Model {

  function get_jml_jenis() {
    return $this->db->query("SELECT * FROM jenis")->num_rows();
  }

  function get_jml_tersedia() {
    return $this->db->query("SELECT * FROM jenis WHERE keterangan='Y'")->num_rows();
  }

  function get_jml_ttersedia() {
    return $this->db->query("SELECT * FROM jenis WHERE keterangan='N'")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM jenis ORDER BY id_jenis DESC");
  }

  function get_edit($post) {
    $this->db->select("id_jenis, kode_jenis, nama_jenis, keterangan");
    $this->db->from("jenis");
    $this->db->where_in('id_jenis', $post['check']);
    $this->db->order_by('id_jenis', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_add($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->insert_batch('jenis', $result);
    }
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('jenis', $result, 'id_jenis');
    }
  }

  function post_delete($post = array()) {
    $total_array = count($post);

    if ($total_array != 0) {
      $this->db->where_in('id_jenis', $post['check']);
      $this->db->delete('jenis');
    }
  }

}