<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_petugas_m');
    $this->load->model('Crud_pegawai_m');
    $this->load->model('Crud_jenis_barang_m');
    $this->load->model('Crud_barang_m');
    $this->load->model('Data_peminjaman_m');
  }

  function index() {
    if ($this->session->userdata('akses') == '1') { //jika statusnya admin yg login

      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('admin/v_dashboard', $q);

    } elseif ($this->session->userdata('akses') == '2') { //jika statusnya operator yg login

      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('operator/v_dashboard', $q);

    } elseif ($this->session->userdata('akses') == '3') { //jika statusnya peminjam yg login
      
      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_pinjam'] = $this->Data_peminjaman_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('peminjam/v_dashboard', $q);

    }
  }

  function data_petugas() {
    redirect(base_url('Crud_petugas'));
  }

  function data_pegawai() {
    redirect(base_url('Crud_pegawai'));
  }

  function data_ruang() {
    redirect(base_url('Crud_ruang'));
  }

  function jenis_barang() {
    redirect(base_url('Crud_jenis_barang'));
  }

  function data_barang() {
    redirect(base_url('Crud_barang'));
  }

  function data_peminjaman() {
    redirect(base_url('Data_peminjaman'));
  }

  function laporan() {
    redirect(base_url('Laporan'));
  }

  function laporan_pinjam() {
    redirect(base_url('Laporan_pinjam'));
  }

}