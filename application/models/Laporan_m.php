<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_m extends CI_Model {

  function view_by_date($date) {
    // $this->db->where('DATE(tanggal_register)', $date); // Tambahkan where tanggal nya
        
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas WHERE DATE(tanggal_register)='$date' ORDER BY id_inventaris DESC")->result(); // Tampilkan data inventaris sesuai tanggal yang diinput oleh user pada filter
  }

  function view_by_month($month, $year) {
    // $this->db->where('MONTH(tanggal_register)', $month); // Tambahkan where bulan
    // $this->db->where('YEAR(tanggal_register)', $year); // Tambahkan where tahun
        
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas WHERE MONTH(tanggal_register)='$month' AND YEAR(tanggal_register)='$year' ORDER BY id_inventaris DESC")->result(); // Tampilkan data inventaris sesuai bulan dan tahun yang diinput oleh user pada filter
  }
    
  function view_by_year($year) {
    // $this->db->where('YEAR(tanggal_register)', $year); // Tambahkan where tahun
        
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas WHERE YEAR(tanggal_register)='$year' ORDER BY id_inventaris DESC")->result(); // Tampilkan data inventaris sesuai tahun yang diinput oleh user pada filter
  }
    
  function view_all() {
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas ORDER BY id_inventaris DESC")->result(); // Tampilkan semua data inventaris
  }
    
  function option_tahun() {
    $this->db->select('YEAR(tanggal_register) AS tahun'); // Ambil Tahun dari field tanggal_register
    $this->db->from('inventaris'); // select ke tabel inventaris
    $this->db->order_by('YEAR(tanggal_register)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
    $this->db->group_by('YEAR(tanggal_register)'); // Group berdasarkan tahun pada field tanggal_register
    
    return $this->db->get()->result(); // Ambil data pada tabel inventaris sesuai kondisi diatas
  }

}