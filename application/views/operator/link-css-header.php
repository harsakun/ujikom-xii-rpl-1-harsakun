  <!-- Logo Favicon -->
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url().'assets/img/logo_inventaris.png' ?>">

  <link rel="stylesheet" href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/css/simple-sidebar2.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery.mCustomScrollbar.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/vendor/fontawesome/css/all.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/vendor/DataTables/datatables.css' ?>">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">