<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Renime">

  <title>CInventoria | Laporan Peminjaman</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>
  
</head>
<body>

<div class="wrapper">

  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar') ?>
    
    <?=$this->session->flashdata('notif');?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item">Laporan</li>
      <li class="breadcrumb-item active">Peminjaman </li>
    </ol>

    <div class="row">

      <div class="col-md-3">
        <div class="card">
          <div class="card-header">
            Filter Berdasarkan
          </div>
          <div class="card-body">
            <form method="get" action="">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <select name="filter" id="filter" class="form-control">
                    <option selected="" disabled="">--Pilih--</option>
                    <option value="1">Per Tanggal</option>
                    <option value="2">Per Bulan</option>
                    <option value="3">Per Tahun</option>
                  </select>
                </div>
                <div class="form-group col-md-12 mb-0">
                  <div id="form-tanggal" class="m-0 p-0">
                    <label>Tanggal</label>
                    <input type="date" name="tanggal" class="input-tanggal form-control">
                  </div>
                </div>
                <div class="form-group col-md-12 pt-0 mb-0">
                  <div id="form-bulan">
                    <label>Bulan</label>
                    <select name="bulan" class="form-control">
                      <option selected="" disabled="">--Pilih--</option>
                      <option value="1">Januari</option>
                      <option value="2">Februari</option>
                      <option value="3">Maret</option>
                      <option value="4">April</option>
                      <option value="5">Mei</option>
                      <option value="6">Juni</option>
                      <option value="7">Juli</option>
                      <option value="8">Agustus</option>
                      <option value="9">September</option>
                      <option value="10">Oktober</option>
                      <option value="11">November</option>
                      <option value="12">Desember</option>
                    </select>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <div id="form-tahun">
                    <label>Tahun</label><br>
                    <select name="tahun" class="form-control">
                      <option selected="" disabled="">--Pilih--</option>
                      <?php
                        foreach($option_tahun as $data){ // Ambil data tahun dari model yang dikirim dari controller
                          echo '<option value="'.$data->tahun.'">'.$data->tahun.'</option>';
                        }
                      ?>
                    </select> 
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-eye"></i> Tampilkan</button>
              <a class="btn btn-success btn-block" href="<?php echo base_url().'Laporan_pinjam' ?>"><i class="fa fa-undo"></i> Reset Filter</a>
            </form>
          </div>
        </div>
      </div>   

      <div class="col-md-9">
        <div class="card">
          <div class="card-header">
            <?php echo $ket; ?>
            <div class="float-right">
              <a class="btn btn-danger btn-sm" href="<?php echo $url_cetak; ?>"><i class="fa fa-file-pdf"></i> CETAK PDF</a>
            </div>
          </div>
          <div class="card-body">    
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="example1">
                <thead class="bg-core">
                  <tr class="text-center">
                    <th>Kode</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Keterangan</th>
                    <th>Penerima</th>
                    <th>Pegawai</th>
                  </tr>
                </thead>
                <?php
                  if(!empty($peminjam)) {
                    $no = 1;
                    foreach($peminjam as $v){
                      $tgl = date('d-M-Y', strtotime($v->tanggal_pinjam));                      
                      echo "<tr>";
                      echo "<td>".$v->kode_inventaris."</td>";
                      echo "<td>".$tgl."</td>";
                      echo "<td>".$v->tanggal_kembali."</td>";
                      echo "<td>".$v->nama."</td>";
                      echo "<td>".$v->jml."</td>";
                      if ($v->status_peminjaman == 'P') {
                        echo "<td><span class='badge badge-primary'>Dipinjam</span></td>";
                      } elseif ($v->status_peminjaman == 'L') {
                        echo "<td><span class='badge badge-primary'>Keperluan lain-lain</span></td>";
                      } elseif ($v->status_peminjaman == 'K') {
                        echo "<td><span class='badge badge-warning'>Sudah Kembali</span></td>";
                      }      
                      echo "<td>".$v->nama_petugas."</td>";
                      echo "<td>".$v->nama_pegawai."</td>";
                      echo "</tr>";
                      $no++;
                    }
                  }
                ?>
              </table>
            </div>
          </div>
        </div>   
      </div>   

    </div>

  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>
  <script type="text/javascript">
    $(document).ready(function(){ // Ketika halaman selesai di load
      $('.input-tanggal').datepicker({
        dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
      });

      $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

      $('#filter').change(function(){ // Ketika user memilih filter
        if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
          $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
          $('#form-tanggal').show(); // Tampilkan form tanggal
        }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
          $('#form-tanggal').hide(); // Sembunyikan form tanggal
          $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
        }else{ // Jika filternya 3 (per tahun)
          $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
          $('#form-tahun').show(); // Tampilkan form tahun
        }

        $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
      })
    });
  </script>

</body>
</html>