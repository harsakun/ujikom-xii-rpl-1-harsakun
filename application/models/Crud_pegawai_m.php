<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_pegawai_m extends CI_Model {

  function get_jml_peminjam() {
    return $this->db->query("SELECT * FROM pegawai")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM pegawai ORDER BY id_pegawai DESC");
  }

  function get_edit($post) {
    $this->db->select("id_pegawai, nip, nama_pegawai, alamat");
    $this->db->from("pegawai");
    $this->db->where_in('id_pegawai', $post['check']);
    $this->db->order_by('id_pegawai', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_add($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->insert_batch('pegawai', $result);
    }
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('pegawai', $result, 'id_pegawai');
    }
  }

  function post_delete($post = array()) {
    $total_array = count($post);

    if ($total_array != 0) {
      $this->db->where_in('id_pegawai', $post['check']);
      $this->db->delete('pegawai');
    }
  }

}