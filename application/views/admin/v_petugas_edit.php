a<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Admin & Operator</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li class="active">
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item">User</li>
      <li class="breadcrumb-item">Admin & Operator</li>
      <li class="breadcrumb-item active">Edit </li>
    </ol>

    <div class="card">
      <div class="card-header">
        Edit Admin & Operator
      </div>
      <div class="card-body">
        <form method="post" action="<?php echo base_url().'Crud_petugas/sunting_proses' ?>">
          <div class="table-responsive pt-2">
            <table class="table table-bordered" id="">
              <thead class="bg-core">
                <tr class="text-center">
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Level</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 1;
                  foreach ($data as $key => $v) { 
                    echo "<tr>
                            <td>".$no++."</td>
                            <input type='hidden' name='id_petugas[]' value='{$v->id_petugas}' class='form-control' required=''>
                            <td><input type='text' name='nama_petugas[]' value='{$v->nama_petugas}' class='form-control' required='' onkeypress='hurufangka(event)'></td>
                            <td><input type='text' name='username[]' value='{$v->username}' class='form-control' required='' onkeypress='hurufangka_ts(event)'></td>
                            ";
                ?>
                            <td>
                              <select name="id_level[]" class='form-control'>
                                <option selected="" disabled="">--Pilih--</option>
                                <?php foreach ($data_level as $l) { ?>
                                  <option value="<?php echo $l->id_level; ?>" <?php if ($l->id_level == $v->id_level) { echo "selected"; } ?> ><?php echo $l->nama_level; ?></option>
                                <?php } ?>
                              </select>
                            </td>
                          </tr>
                <?php            
                  }
                ?>
              </tbody>
            </table>
            <a href="<?php echo base_url().'crud_petugas/' ?>"class="btn btn-primary"><i class="fa fa-arrow-alt-circle-left"></i> Batal</a>
            <button type="submit" name="sunting" value="simpan perubahan" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
            <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>
  
  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>

</body>
</html>