<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Renime">

  <title>CInventoria | Peminjaman</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>
  
</head>
<body>

<div class="wrapper">

  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Peminjam</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li class="active">
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/Laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar') ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item active">Barang Pinjam</li>
    </ol>

    <div class="bd-callout bd-callout-warning">
      <h4 id="third-party-libraries">Catatan :</h4>
      <p><strong>* Harap clik tombol <a class="badge badge-primary text-white">Cari</a> setelah menginputkan kode barang.</strong><br><strong>* Harap periksa lagi form input peminjaman.</strong></p>
    </div>
    
    <?=$this->session->flashdata('notif');?>

    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-header">
            Peminjaman
          </div>
          <div class="card-body">
            <form method="post" action="<?php echo base_url().'Data_peminjaman/tambah_proses1' ?>">
              <?php 
                date_default_timezone_set('asia/jakarta');
                $tgl = date('Y-m-d');
                $petugas = $this->session->userdata('ses_id');
              ?>
              <span class="badge badge-primary">Form</span>
              <div class="float-right">
                <input type='text' name='tanggal_pinjam' value='<?php echo $tgl; ?>' class='form-control' required='' readonly=''>
              </div>
              <hr>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Kode Barang</label>
                  <div class="input-group">
                    <input type='text' name='' id="kode_inventaris" class='form-control' required='' autocomplete='off' onkeypress='stripe(event)'>
                    <button type="button" class="btn btn-primary" id="btn-search">Cari</button>
                    <i class="text-muted" style="font-size: 10pt;">*Masukan kode barang</i>
                  </div>
                  <span id="loading">LOADING...</span>
                </div>
                <div class="form-group col-md-6">
                  <label>Nama Barang</label>
                  <input type='text' name='' id="nama" class='form-control' readonly="" required='' autocomplete='off'>
                  <input type='hidden' name='id_inventaris' id="id_inventaris" class='form-control' readonly="" required='' autocomplete='off'>
                </div>
                <div class="form-group col-md-6">
                  <label>Keterangan</label>
                  <select name="status_peminjaman" class="form-control">
                    <option selected="" disabled="">-- Pilih --</option>
                    <option value="P">Dipinjam</option>
                    <option value="L">Keperluan Lain - Lain</option>
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label>Jumlah</label>
                  <input type='text' name='jml' class='form-control' required='' autocomplete='off'
                  onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                  onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
                </div>
                <div class="col-md-6">
                  <label>Penerima</label>
                  <select name="id_petugas" class='form-control'>
                    <option selected="" disabled="">-- Pilih --</option>
                    <?php foreach ($data_petugas as $v) { ?>
                      <option value="<?php echo $v->id_petugas; ?>"><?php echo $v->nama_petugas; ?> | <?php echo $v->nama_level; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Pegawai</label>
                  <select name="id_pegawai" class='form-control'>
                    <option selected="" disabled="">-- Pilih --</option>
                    <?php foreach ($data_pegawai as $v) { ?>
                      <option value="<?php echo $v->id_pegawai; ?>"><?php echo $v->nama_pegawai; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <hr>
              
              <button type="submit" name="simpan" value="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
              <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-7">
        <div class="card">
          <div class="card-header">
            Data Barang
          </div>
          <div class="card-body">
            <div class="table-responsive pt-2">
              <table class="table table-bordered" id="example1">
                <thead class="bg-core">
                  <tr class="text-center">
                    <th width="10">No</th>
                    <th>Kode inventaris</th>
                    <th>Nama</th>
                    <th>Spesifikasi</th>
                    <th>Kondisi</th>
                    <th>Stok</th>
                    <th>Jenis</th>
                    <th>Tempat</th>
                    <th>Keterangan</th>
                    <th>Petugas</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if ($data_count == 0) {
                      echo "data kosong";
                    } else {
                      $no = 1;
                      foreach ($data as $v) {
                        echo "<tr>";
                        echo "<td class='text-center'>".$no++."</td>";
                        echo "<td>".$v->kode_inventaris."</td>";
                        echo "<td>".$v->nama."</td>";
                        echo "<td>".$v->spesifikasi."</td>";
                        echo "<td>".$v->kondisi."</td>";
                        echo "<td>".$v->jumlah."</td>";
                        echo "<td>".$v->nama_jenis."</td>";
                        echo "<td>".$v->nama_ruang."</td>";
                        if ($v->ket == 'Y') {
                          echo "<td><span class='badge badge-primary'>Tersedia</span></td>";
                        } else {
                          echo "<td><span class='badge badge-warning'>Tidak Tersedia</span></td>";
                        }                         
                        echo "<td>".$v->nama_petugas."</td>";
                        echo "</tr>";
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12 pt-4">
        <div class="card">
          <div class="card-header">
            Data Peminjaman
          </div>
          <div class="card-body">
            <div class="table-responsive pt-2">
              <table class="table table-bordered" id="example2">
                <thead class="bg-core">
                  <tr class="text-center">
                    <th width="10">No</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Penerima</th>
                    <th>Pegawai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no = 1;
                    foreach ($data_peminjam as $v) {
                      echo "<tr>";
                      echo "<td class='text-center'>".$no++."</td>";
                      echo "<td>".date('Y-M-d', strtotime($v->tanggal_pinjam))."</td>";
                      echo "<td>".date('Y-M-d', strtotime($v->tanggal_kembali))."</td>";
                      echo "<td>".$v->nama."</td>";
                      echo "<td>".$v->jml."</td>";
                      echo "<td>".$v->nama_petugas."</td>";
                      echo "<td>".$v->nama_pegawai."</td>";
                      if ($v->status_peminjaman == 'P') {
                        echo "<td><a href='".base_url('Data_peminjaman/update/'.$v->id_detail_pinjam)."' class='btn btn-danger'>Dipinjam</a></td>";
                      } elseif ($v->status_peminjaman == 'L') {
                        echo "<td><a href='".base_url('Data_peminjaman/update/'.$v->id_detail_pinjam)."' class='btn btn-warning'>Keperluan lain-lain</a></td>";
                      } else {
                        echo "<td><a class='btn btn-primary disabled'>Sudah Kembali</a></td>";
                      }                         
                      echo "</tr>";
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>

</body>
</html>