<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_pegawai extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_pegawai_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') { //jika statusnya admin yg login

      $q['data'] = $this->Crud_pegawai_m->get_list()->result();
      $q['data_count'] = $this->Crud_pegawai_m->get_list()->num_rows();
      $this->load->view('admin/v_pegawai', $q);

    } elseif ($this->session->userdata('akses') == '2') { //jika statusnya operator

      $q['data'] = $this->Crud_pegawai_m->get_list()->result();
      $q['data_count'] = $this->Crud_pegawai_m->get_list()->num_rows();
      $this->load->view('operator/v_pegawai', $q);

    } elseif ($this->session->userdata('akses') == '3') {
      $this->load->view('error404');
    }
    
  }

  function tambah() {

    if ($this->session->userdata('akses') == '1') { //jika statusnya admin

      $get = $this->input->get();
      $q['total_form'] = $get['total_form'];
      $this->load->view('admin/v_pegawai_tambah', $q);

    } elseif ($this->session->userdata('akses') == '2') { //jika statusnya operator

      $get = $this->input->get();
      $q['total_form'] = $get['total_form'];
      $this->load->view('operator/v_pegawai_tambah', $q);

    } 

  }

  function tambah_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['nama_pegawai']);

    foreach ($post['nama_pegawai'] as $key => $val) {
      $result[] = array(
        "nip" => $post['nip'][$key],
        "nama_pegawai" => $post['nama_pegawai'][$key],
        "alamat" => $post['alamat'][$key]
      );
    }
    $this->Crud_pegawai_m->post_add($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di tambahkan!</div>');
    redirect('Crud_pegawai');
  }

  function sunting_hapus() {
    $post = $this->input->post();
    $check = $post['check'];

    if (isset($check)) {
      if (isset($post['sunting'])) {
        if ($this->session->userdata('akses') == '1') {

          $q['data'] = $this->Crud_pegawai_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_pegawai_m->get_edit($post)->num_rows();

          $this->load->view('admin/v_pegawai_edit', $q);
        } elseif ($this->session->userdata('akses') == '2') {
          
          $q['data'] = $this->Crud_pegawai_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_pegawai_m->get_edit($post)->num_rows();

          $this->load->view('operator/v_pegawai_edit', $q);
        }
        
      } elseif (isset($post['hapus'])) {
        $this->Crud_pegawai_m->post_delete($post);

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
          .count($check).' data berhasil di hapus!</div>');
        redirect('Crud_pegawai');
      }
    } else {
      $this->session->set_flashdata('notif', '<div class="alert alert-primary" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Harap centang dulu datanya!</div>');
      redirect('Crud_pegawai');
    }
  }

  function sunting_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['id_pegawai']);

    foreach ($post['id_pegawai'] as $key => $val) {
      $result[] = array(
        "id_pegawai" => $post['id_pegawai'][$key],
        "nip" => $post['nip'][$key],
        "nama_pegawai" => $post['nama_pegawai'][$key],
        "alamat" => $post['alamat'][$key]
      );
    }
    $this->Crud_pegawai_m->post_edit($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di perbarui!</div>');
    redirect('Crud_pegawai');
  }

}