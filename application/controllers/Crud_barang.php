<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_barang extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_barang_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') {
      
      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('admin/v_barang', $q);

    } elseif ($this->session->userdata('akses') == '2') {
      
      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('operator/v_barang', $q);

    } elseif ($this->session->userdata('akses') == '3') {
      $this->load->view('error404');
    }
    
  }

  function tambah() {

    if ($this->session->userdata('akses') == '1') {
      
      $get = $this->input->get();
      $q['data_jenis'] = $this->Crud_barang_m->get_jenis()->result();
      $q['data_ruang'] = $this->Crud_barang_m->get_ruang()->result();
      $q['total_form'] = $get['total_form'];
      $this->load->view('admin/v_barang_tambah', $q);

    } elseif ($this->session->userdata('akses') == '2') {
      
      $get = $this->input->get();
      $q['data_jenis'] = $this->Crud_barang_m->get_jenis()->result();
      $q['data_ruang'] = $this->Crud_barang_m->get_ruang()->result();
      $q['total_form'] = $get['total_form'];
      $this->load->view('operator/v_barang_tambah', $q);

    }
    
  }

  function tambah_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['kode_inventaris']);

    foreach ($post['kode_inventaris'] as $key => $val) {

      if (empty($post['kondisi']) && empty($post['id_jenis']) && empty($post['id_ruang']) && empty($post['keterangan'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data kondisi, jenis, tempat, dan keterangan tidak boleh kosong!</div>');
        redirect('Crud_barang/tambah?total_form=1&submit=ok');
      } elseif (empty($post['kondisi'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data kondisi tidak boleh kosong!</div>');
        redirect('Crud_barang/tambah?total_form=1&submit=ok');
      } elseif (empty($post['id_jenis'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data jenis tidak boleh kosong!</div>');
        redirect('Crud_barang/tambah?total_form=1&submit=ok');
      } elseif (empty($post['id_ruang'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data tempat tidak boleh kosong!</div>');
        redirect('Crud_barang/tambah?total_form=1&submit=ok');
      } elseif (empty($post['keterangan'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data keterangan tidak boleh kosong!</div>');
        redirect('Crud_barang/tambah?total_form=1&submit=ok');
      } else {
        $result[] = array(
          "kode_inventaris" => $post['kode_inventaris'][$key],
          "nama" => $post['nama'][$key],
          "jumlah" => $post['jumlah'][$key],
          "spesifikasi" => $post['spesifikasi'][$key],
          "kondisi" => $post['kondisi'][$key],
          "id_jenis" => $post['id_jenis'][$key],
          "id_ruang" => $post['id_ruang'][$key],
          "ket" => $post['keterangan'][$key],
          "tanggal_register" => $post['tanggal_register'][$key],
          "id_petugas" => $post['id_petugas'][$key]
        );
      }
        
    }
    $this->Crud_barang_m->post_add($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di tambahkan!</div>');
    redirect('Crud_barang');
  }

  function sunting_hapus() {
    $post = $this->input->post();
    $check = $post['check'];

    if (isset($check)) {
      if (isset($post['sunting'])) {
        if ($this->session->userdata('akses') == '1') {
      
          $q['data_jenis'] = $this->Crud_barang_m->get_jenis()->result();
          $q['data_ruang'] = $this->Crud_barang_m->get_ruang()->result();
          $q['data'] = $this->Crud_barang_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_barang_m->get_edit($post)->num_rows();

          $this->load->view('admin/v_barang_edit', $q);

        } elseif ($this->session->userdata('akses') == '2') {
          
          $q['data_jenis'] = $this->Crud_barang_m->get_jenis()->result();
          $q['data_ruang'] = $this->Crud_barang_m->get_ruang()->result();
          $q['data'] = $this->Crud_barang_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_barang_m->get_edit($post)->num_rows();

          $this->load->view('operator/v_barang_edit', $q);

        }
      } elseif (isset($post['hapus'])) {
        $this->Crud_barang_m->post_delete($post);

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
          .count($check).' data berhasil di hapus!</div>');
        redirect('Crud_barang');
      }
    } else {
      $this->session->set_flashdata('notif', '<div class="alert alert-primary" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Harap centang dulu datanya!</div>');
      redirect('Crud_barang');
    }
  }

  function sunting_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['id_inventaris']);

    foreach ($post['id_inventaris'] as $key => $val) {
      $result[] = array(
        "id_inventaris" => $post['id_inventaris'][$key],
        "kode_inventaris" => $post['kode_inventaris'][$key],
        "nama" => $post['nama'][$key],
        "jumlah" => $post['jumlah'][$key],
        "spesifikasi" => $post['spesifikasi'][$key],
        "kondisi" => $post['kondisi'][$key],
        "id_jenis" => $post['id_jenis'][$key],
        "id_ruang" => $post['id_ruang'][$key],
        "ket" => $post['keterangan'][$key]
      );
    }
    $this->Crud_barang_m->post_edit($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di perbarui!</div>');
    redirect('Crud_barang');
  }

}