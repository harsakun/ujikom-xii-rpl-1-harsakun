<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_barang_m extends CI_Model {

  function get_jenis() {
    return $this->db->query('SELECT * FROM jenis ORDER BY nama_jenis ASC');
  }

  function get_ruang() {
    return $this->db->query('SELECT * FROM ruang ORDER BY nama_ruang ASC');
  }

  function get_jml_barang() {
    return $this->db->query("SELECT * FROM inventaris")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas ORDER BY id_inventaris DESC");
  }

  function get_inv($id) {
    return $this->db->query("SELECT * FROM inventaris invn INNER JOIN jenis je ON invn.id_jenis=je.id_jenis JOIN ruang ru ON invn.id_ruang=ru.id_ruang JOIN petugas pe ON invn.id_petugas=pe.id_petugas WHERE id_inventaris='$id' ORDER BY id_inventaris DESC");
  }

  function get_edit($post) {
    $this->db->select("id_inventaris, kode_inventaris, nama, jumlah, spesifikasi, kondisi, id_jenis, id_ruang, ket");
    $this->db->from("inventaris");
    $this->db->where_in('id_inventaris', $post['check']);
    $this->db->order_by('id_inventaris', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_add($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->insert_batch('inventaris', $result);
    }
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('inventaris', $result, 'id_inventaris');
    }
  }

  function post_delete($post = array()) {
    $total_array = count($post);

    if ($total_array != 0) {
      $this->db->where_in('id_inventaris', $post['check']);
      $this->db->delete('inventaris');
    }
  }

}