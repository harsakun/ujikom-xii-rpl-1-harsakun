function hurufangkacombine(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[a-zA-Z0-9, ?.!]/.test(ch))){
       evt.preventDefault();
    }
}
function stripe(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[a-zA-Z0-9, _-]/.test(ch))){
       evt.preventDefault();
    }
}
function hurufangka(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[a-zA-Z0-9 ]/.test(ch))){
       evt.preventDefault();
    }
}
function hurufangka_ts(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[a-zA-Z0-9]/.test(ch))){
        evt.preventDefault();
    }
}

function angka(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[0-9 ]/.test(ch))){
       evt.preventDefault();
    }
}
function angka_ts(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[0-9]/.test(ch))){
        evt.preventDefault();
    }
}

function email_ts(evt){
    var ch = String.fromCharCode(evt.which);
    if(!(/[a-zA-Z0-9,?.!@_-]/.test(ch))){
       evt.preventDefault();
    }
}

