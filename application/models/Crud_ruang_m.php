<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_ruang_m extends CI_Model {

  function get_jml_ruang() {
    return $this->db->query("SELECT * FROM ruang")->num_rows();
  }

  function get_jml_tersedia() {
    return $this->db->query("SELECT * FROM ruang WHERE keterangan='Y'")->num_rows();
  }

  function get_jml_ttersedia() {
    return $this->db->query("SELECT * FROM ruang WHERE keterangan='N'")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM ruang ORDER BY id_ruang DESC");
  }

  function get_edit($post) {
    $this->db->select("id_ruang, kode_ruang, nama_ruang, keterangan");
    $this->db->from("ruang");
    $this->db->where_in('id_ruang', $post['check']);
    $this->db->order_by('id_ruang', 'DESC');
    $q = $this->db->get();

    return $q;
  }

  function post_add($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->insert_batch('ruang', $result);
    }
  }

  function post_edit($result = array()) {
    $total_array = count($result);

    if ($total_array != 0) {
      $this->db->update_batch('ruang', $result, 'id_ruang');
    }
  }

  function post_delete($post = array()) {
    $total_array = count($post);

    if ($total_array != 0) {
      $this->db->where_in('id_ruang', $post['check']);
      $this->db->delete('ruang');
    }
  }

}