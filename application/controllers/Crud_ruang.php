<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_ruang extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_ruang_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') {
      
      $q['data'] = $this->Crud_ruang_m->get_list()->result();
      $q['data_count'] = $this->Crud_ruang_m->get_list()->num_rows();
      $this->load->view('admin/v_ruang', $q);

    } elseif ($this->session->userdata('akses') == '2') {
      
      $q['data'] = $this->Crud_ruang_m->get_list()->result();
      $q['data_count'] = $this->Crud_ruang_m->get_list()->num_rows();
      $this->load->view('operator/v_ruang', $q);

    }elseif ($this->session->userdata('akses') == '3') {
      $this->load->view('error404');
    }
    
  }

  function tambah() {

    if ($this->session->userdata('akses') == '1') {
      
      $get = $this->input->get();
      $q['total_form'] = $get['total_form'];
      $this->load->view('admin/v_ruang_tambah', $q);

    } elseif ($this->session->userdata('akses') == '2') {
      
      $get = $this->input->get();
      $q['total_form'] = $get['total_form'];
      $this->load->view('operator/v_ruang_tambah', $q);

    }
    
  }

  function tambah_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['kode_ruang']);

    foreach ($post['kode_ruang'] as $key => $val) {

      if (empty($post['keterangan'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data keterangan tidak boleh kosong!</div>');
        redirect('Crud_ruang/tambah?total_form=1&submit=ok');
      } else {
        $result[] = array(
          "kode_ruang" => $post['kode_ruang'][$key],
          "nama_ruang" => $post['nama_ruang'][$key],
          "keterangan" => $post['keterangan'][$key]          
        );
      }

    }
    $this->Crud_ruang_m->post_add($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di tambahkan!</div>');
    redirect('Crud_ruang');
  }

  function sunting_hapus() {
    $post = $this->input->post();
    $check = $post['check'];

    if (isset($check)) {
      if (isset($post['sunting'])) {
        if ($this->session->userdata('akses') == '1') {
      
          $q['data'] = $this->Crud_ruang_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_ruang_m->get_edit($post)->num_rows();

          $this->load->view('admin/v_ruang_edit', $q);

        } elseif ($this->session->userdata('akses') == '2') {
          
          $q['data'] = $this->Crud_ruang_m->get_edit($post)->result();
          $q['data_count'] = $this->Crud_ruang_m->get_edit($post)->num_rows();

          $this->load->view('operator/v_ruang_edit', $q);

        }
      } elseif (isset($post['hapus'])) {
        $this->Crud_ruang_m->post_delete($post);

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
          .count($check).' data berhasil di hapus!</div>');
        redirect('Crud_ruang');
      }
    } else {
      $this->session->set_flashdata('notif', '<div class="alert alert-primary" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Harap centang dulu datanya!</div>');
      redirect('Crud_ruang');
    }
  }

  function sunting_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['id_ruang']);

    foreach ($post['id_ruang'] as $key => $val) {
      $result[] = array(
        "id_ruang" => $post['id_ruang'][$key],
        "kode_ruang" => $post['kode_ruang'][$key],
        "nama_ruang" => $post['nama_ruang'][$key],
        "keterangan" => $post['keterangan'][$key]
      );
    }
    $this->Crud_ruang_m->post_edit($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di perbarui!</div>');
    redirect('Crud_ruang');
  }

}