<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Renime">

  <title>CInventoria | Dashboard</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('peminjam/link-css-header'); ?>
  
</head>
<body>

<div class="wrapper">

  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li class="active">
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman/' ?>"><i class="fa fa-credit-card mr-2"></i> Peminjaman</a>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('peminjam/sidebar-foot'); ?>
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('peminjam/navbar') ?>

    <?=$this->session->flashdata('notif');?>

    <div class="jumbotron text-center bg-white p-4">
      <h1 class="display-6">Selamat Datang di CInventoria!<h1>
      <p class="lead">--- anda telah login sebagai peminjam ---</p>
    </div>

    <div class="card">
      <div class="card-header">
        Data Peminjaman
      </div>
      <div class="card-body">
        <div class="table-responsive pt-2">
          <table class="table table-bordered" id="example1">
            <thead class="bg-core">
              <tr class="text-center">
                <th width="10">No</th>
                <th>Tanggal Pinjam</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <th>Keterangan</th>
                <th>Pegawai</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $no = 1;
                foreach ($data_pinjam as $v) {
                  $tgl = date('d-M-Y', strtotime($v->tanggal_pinjam));
                  echo "<tr class='text-center'>";
                  echo "<td class='text-center'>".$no++."</td>";
                  echo "<td>".$tgl."</td>";
                  echo "<td>".$v->nama."</td>";
                  echo "<td>".$v->jml."</td>";
                  if ($v->status_peminjaman == 'P') {
                    echo "<td><span class='badge badge-danger'>Dipinjam</span></td>";
                  } elseif ($v->status_peminjaman == 'L') {
                    echo "<td><span class='badge badge-warning'>Keperluan lain-lain</span></td>";
                  } else {
                    echo "<td><span class='badge badge-primary'>Sudah Kembali</span></td>";
                  }                        
                  echo "<td>".$v->nama_pegawai."</td>";
                  echo "</tr>";
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('peminjam/link-js-footer'); ?>

</body>
</html>