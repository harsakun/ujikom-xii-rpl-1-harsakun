<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_peminjaman extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_barang_m');
    $this->load->model('Crud_petugas_m');
    $this->load->model('Crud_pegawai_m');
    $this->load->model('Data_peminjaman_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') {

      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_peminjam'] = $this->Data_peminjaman_m->get_list()->result();
      $q['data_petugas'] = $this->Crud_petugas_m->get_list()->result();
      $q['data_pegawai'] = $this->Crud_pegawai_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('admin/v_peminjaman', $q);

    } elseif ($this->session->userdata('akses') == '2') {

      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_peminjam'] = $this->Data_peminjaman_m->get_list()->result();
      $q['data_petugas'] = $this->Crud_petugas_m->get_list()->result();
      $q['data_pegawai'] = $this->Crud_pegawai_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('operator/v_peminjaman', $q);

    } elseif ($this->session->userdata('akses') == '3') {

      $q['data'] = $this->Crud_barang_m->get_list()->result();
      $q['data_pegawai'] = $this->Crud_pegawai_m->get_list()->result();
      $q['data_count'] = $this->Crud_barang_m->get_list()->num_rows();
      $this->load->view('peminjam/v_peminjaman', $q);

    }
    
  }

  function search() {
    // Ambil data NIS yang dikirim via ajax post
    $kode = $this->input->post('kode_inventaris');    
    $barang = $this->Data_peminjaman_m->viewByNis($kode);
    
    if (!empty($barang)) { // Jika data siswa ada/ditemukan
      // Buat sebuah array
      $callback = array(
        'status' => 'success', // Set array status dengan success
        'nama' => $barang->nama, // Set array nama dengan isi kolom nama pada tabel siswa
        'id_inventaris' => $barang->id_inventaris, // Set array nama dengan isi kolom nama pada tabel siswa
      );
    } else {
      $callback = array('status' => 'failed'); // set array status dengan failed
    }

    echo json_encode($callback); // konversi varibael $callback menjadi JSON
  }

  function tambah_proses() {
    $post = $this->input->post();

    $id = $post['id_inventaris'];
    $show = $this->Crud_barang_m->get_inv($id)->result();

    foreach ($show as $v) {
      $v->jumlah;
    
      if ($post['jml'] > $v->jumlah ) {

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena jumlah barang yang akan anda pinjam, melebihi dari stok barang yang tersedia!</div>');
        redirect('Data_peminjaman');

      }  elseif (empty($status = $post['status_peminjaman']) && empty($pegawai = $post['id_pegawai'])) {

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input keterangan dan pegawai masih kosong!</div>');
        redirect('Data_peminjaman');

      } elseif (empty($status = $post['status_peminjaman'])) {
        
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input keterangan masih kosong!</div>');
        redirect('Data_peminjaman');

      } elseif (empty($pegawai = $post['id_pegawai'])) {
        
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input pegawai masih kosong!</div>');
        redirect('Data_peminjaman');

      } else {
          
        $id = $post['id_inventaris'];
        $jumlah = $post['jml'];
        $status = $post['status_peminjaman'];
        $petugas = $post['id_petugas'];
        $data1 = array(
                        'id_inventaris'=>$id,
                        'jml'=>$jumlah,
                        'status_peminjaman'=>$status,
                        'tanggal_kembali'=>0,
                        'id_petugas'=>$petugas
                      );
        $insert = $this->Data_peminjaman_m->create('detail_pinjam', $data1);

        $lastid = $this->db->insert_id();

        $tgl_pinjam = $post['tanggal_pinjam'];
        $pegawai = $post['id_pegawai'];
        $data = array(
                        'tanggal_pinjam'=>$tgl_pinjam,
                        'id_detail_pinjam'=>$lastid,
                        'id_pegawai'=>$pegawai,
                      );
        $insert1 = $this->Data_peminjaman_m->create('peminjaman', $data);

        $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Berhasil Meminjam Barang!</div>');
        redirect('Data_peminjaman');

      }  
    }
  }

  function tambah_proses1() {
    $post = $this->input->post();
    
    $id = $post['id_inventaris'];
    $show = $this->Crud_barang_m->get_inv($id)->result();

    foreach ($show as $v) {
      $v->jumlah;  

      if ($post['jml'] > $v->jumlah ) {

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena jumlah barang yang akan anda pinjam, melebihi dari stok barang yang tersedia!</div>');
        redirect('Data_peminjaman');

      }  elseif (empty($status = $post['status_peminjaman']) && empty($petugas = $post['id_petugas']) && empty($pegawai = $post['id_pegawai'])) {

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input keterangan, penerima, dan pegawai masih kosong!</div>');
        redirect('Data_peminjaman');

      } elseif (empty($status = $post['status_peminjaman'])) {
        
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input keterangan masih kosong!</div>');
        redirect('Data_peminjaman');

      } elseif (empty($petugas = $post['id_petugas'])) {
        
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input penerima masih kosong!</div>');
        redirect('Data_peminjaman');

      } elseif (empty($pegawai = $post['id_pegawai'])) {
        
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Gagal meminjam, karena form input pegawai masih kosong!</div>');
        redirect('Data_peminjaman');

      } else {

        $id = $post['id_inventaris'];
        $jumlah = $post['jml'];
        $status = $post['status_peminjaman'];
        $petugas = $post['id_petugas'];
        $data1 = array(
                        'id_inventaris'=>$id,
                        'jml'=>$jumlah,
                        'status_peminjaman'=>$status,
                        'tanggal_kembali'=>0,
                        'id_petugas'=>$petugas
                      );
        $insert = $this->Data_peminjaman_m->create('detail_pinjam', $data1);

        $lastid = $this->db->insert_id();
        
        $tgl_pinjam = $post['tanggal_pinjam'];
        $petugas = $post['id_petugas'];
        $pegawai = $post['id_pegawai'];
        $data = array(
                        'tanggal_pinjam'=>$tgl_pinjam,
                        'id_detail_pinjam'=>$lastid,
                        'id_pegawai'=>$petugas,
                        'id_pegawai'=>$pegawai
                      );
        $insert1 = $this->Data_peminjaman_m->create('peminjaman', $data);


        $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Berhasil Meminjam Barang!</div>');
        redirect('Data_peminjaman');
      }
    }
  }

  function update($id) {
    $post = $this->input->post();

    date_default_timezone_set('asia/jakarta');
    $tgl = date('Y m d');
    $data = array(
      'status_peminjaman' => 'K',
    );
   
    $where = array(
      'id_detail_pinjam' => $id
    );
   
    $this->Data_peminjaman_m->update_data($where,$data,'detail_pinjam');

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Berhasil Mengembalikan Barang!</div>');
    redirect('Data_peminjaman');
  }

}