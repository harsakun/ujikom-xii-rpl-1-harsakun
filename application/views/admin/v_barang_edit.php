<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CInventoria | Barang</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>

</head>
<body>

<div class="wrapper">
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li>
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li class="active">
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>    
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar'); ?>

    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url().'Page/' ?>">Dashboard</a></li>
      <li class="breadcrumb-item">Barang</li>
      <li class="breadcrumb-item active">Edit </li>
    </ol>

    <div class="card">
      <div class="card-header">
        Edit Barang
      </div>
      <div class="card-body">
        <form method="post" action="<?php echo base_url().'Crud_barang/sunting_proses' ?>">
          <?php 
            $this->load->helper('string');
            date_default_timezone_set('asia/jakarta');
            $tgl = date('Y-m-d');
            $petugas = $this->session->userdata('ses_id');
            $no = 1;
            foreach ($data as $key => $v) { 
          ?>
            <span class="badge badge-primary">Data Ke - <?php echo $no++; ?></span>
            <hr>
            <input type="hidden" name="id_inventaris[]" value="<?php echo $v->id_inventaris; ?>">
            <div class="form-row">
              <div class="form-group col-md-3">
                <label>Kode Barang</label>
                <input type='text' name='kode_inventaris[]' value='<?php echo $v->kode_inventaris; ?>' class='form-control' required='' readonly=''>
                <small class="form-text text-muted">* Kode sudah terisi secara otomatis</small>
              </div>
              <div class="form-group col-md-6">
                <label>Nama Barang</label>
                <input type='text' name='nama[]' value="<?php echo $v->nama; ?>" class='form-control' required='' autocomplete='off' onkeypress='hurufangka(event)'>
              </div>
              <div class="form-group col-md-3">
                <label>Jumlah</label>
                <input type='text' name='jumlah[]' value="<?php echo $v->jumlah; ?>" class='form-control' required='' autocomplete='off'
                onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
              </div>
            </div>
            <div class="form-group">
              <label>Spesifikasi</label>
              <textarea name='spesifikasi[]' rows="3" class='form-control' required='' onkeypress='hurufangka(event)'><?php echo $v->spesifikasi; ?></textarea>
            </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label>Kondisi</label>
                <select name='kondisi[]' class='form-control' required=''>
                  <option selected='' disabled=''>--Pilih--</option>
                  <option value='Baik' <?php if ($v->kondisi == 'Baik') {echo "selected";} ?> >Baik</option>
                  <option value='Rusak' <?php if ($v->kondisi == 'Rusak') {echo "selected";} ?> >Rusak</option>
                  <option value='Rusak Parah' <?php if ($v->kondisi == 'Rusak Parah') {echo "selected";} ?>>Rusak Parah</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Jenis</label>
                <select name="id_jenis[]" class='form-control'>
                  <option selected="" disabled="">--Pilih--</option>
                  <?php foreach ($data_jenis as $j) { ?>
                    <option value="<?php echo $j->id_jenis; ?>" <?php if ($j->id_jenis == $v->id_jenis) { echo "selected"; } ?> ><?php echo $j->nama_jenis; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Tempat</label>
                <select name="id_ruang[]" class='form-control'>
                  <option selected="" disabled="">--Pilih--</option>
                  <?php foreach ($data_ruang as $r) { ?>
                    <option value="<?php echo $r->id_ruang; ?>" <?php if ($r->id_ruang == $v->id_ruang) { echo "selected"; } ?> ><?php echo $r->nama_ruang; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label>Keterangan</label>
                <select name='keterangan[]' class='form-control' required=''>
                  <option selected='' disabled=''>--Pilih--</option>
                  <option value='Y' <?php if ($v->ket == 'Y') {echo "selected";} ?>>Tersedia</option>
                  <option value='N' <?php if ($v->ket == 'N') {echo "selected";} ?>>Tidak Tersedia</option>
                </select>
              </div>
            </div>
            <hr>
            <?php            
              }
            ?>        
            
            <a href="<?php echo base_url().'Crud_barang/' ?>"class="btn btn-primary"><i class="fa fa-arrow-alt-circle-left"></i> Batal</a>
            <button type="submit" name="simpan" value="simpan perubahan" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
            <button type="reset" value="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>
  
  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>

</body>
</html>