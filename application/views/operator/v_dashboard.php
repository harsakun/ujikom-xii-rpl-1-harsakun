<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Renime">

  <title>CInventoria | Dashboard</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('operator/link-css-header'); ?>
  
</head>
<body>

<div class="wrapper">

  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li class="active">
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('operator/sidebar-foot'); ?>
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('operator/navbar') ?>

    <?=$this->session->flashdata('notif');?>

    <div class="jumbotron text-center bg-white p-4">
      <h1 class="display-6">Selamat Datang di CInventoria!<h1>
      <p class="lead">--- anda telah login sebagai operator ---</p>
    </div>

    <div class="card">
      <div class="card-header">
        Data Barang
      </div>
      <div class="card-body">
        <div class="table-responsive pt-2">
          <table class="table table-bordered" id="example1">
            <thead class="bg-core">
              <tr class="text-center">
                <th width="10">No</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Spesifikasi</th>
                <th>Kondisi</th>
                <th>Jumlah</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Keterangan</th>
                <th>Petugas</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                if ($data_count == 0) {
                  echo "data kosong";
                } else {
                  $no = 1;
                  foreach ($data as $v) {
                    echo "<tr class='text-center'>";
                    echo "<td class='text-center'>".$no++."</td>";
                    echo "<td>".$v->kode_inventaris."</td>";
                    echo "<td>".$v->nama."</td>";
                    echo "<td>".$v->spesifikasi."</td>";
                    echo "<td>".$v->kondisi."</td>";
                    echo "<td>".$v->jumlah."</td>";
                    echo "<td>".$v->nama_jenis."</td>";
                    echo "<td>".$v->nama_ruang."</td>";
                    if ($v->ket == 'Y') {
                      echo "<td><span class='badge badge-primary'>Tersedia</span></td>";
                    } else {
                      echo "<td><span class='badge badge-warning'>Tidak Tersedia</span></td>";
                    }                         
                    echo "<td>".$v->nama_petugas."</td>";
                    echo "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('operator/link-js-footer'); ?>

</body>
</html>