<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('M_login');
  }

  function index() {
    $this->load->view('v_login');
  }

  function auth() {
    $username = htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
    $password = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

    $cek_petugas = $this->M_login->auth_petugas($username,$password);

    if ($cek_petugas->num_rows() > 0) { //jika login sebagai petugas
      
      $data = $cek_petugas->row_array();
      $this->session->set_userdata('masuk',TRUE);

      if ($data['id_level'] == '1') { //akses admin
        $this->session->set_userdata('akses','1');
        $this->session->set_userdata('ses_id',$data['id_petugas']);
        $this->session->set_userdata('ses_name',$data['nama_petugas']);
        redirect('Page/');
      } elseif ($data['id_level'] == '2') { //akses operator
        $this->session->set_userdata('akses','2');
        $this->session->set_userdata('ses_id',$data['id_petugas']);
        $this->session->set_userdata('ses_name',$data['nama_petugas']);
        redirect('Page/');
      } elseif ($data['id_level'] == '3') { //akses peminjam
        $this->session->set_userdata('akses','3');
        $this->session->set_userdata('ses_id',$data['id_petugas']);
        $this->session->set_userdata('ses_name',$data['nama_petugas']);
        redirect('Page/');
      }
    } else {
      $url=base_url();
      echo $this->session->set_flashdata('msg','<div class="alert alert-danger" style="color: #fff;background-color: #ec292a;">
        Username Atau Password Salah!
      </div>');
      redirect($url);
    }
  }

  function logout() {
    $this->session->sess_destroy();
    $url = base_url('');
    redirect($url);
  }

  function sunting_profile() {
    $post = $this->input->post();
    if (isset($post['id_petugas']) or isset($post['id_pegawai'])) {
      if (isset($post['sunting'])) {
        if ($this->session->userdata('akses') == '1') {
      
          $q['data_level'] = $this->M_login->get_level()->result();
          $q['data'] = $this->M_login->get_edit($post)->result();
          $q['data_count'] = $this->M_login->get_edit($post)->num_rows();

          $this->load->view('admin/profile', $q);

        } elseif ($this->session->userdata('akses') == '2') {
          
          $q['data_level'] = $this->M_login->get_level()->result();
          $q['data'] = $this->M_login->get_edit($post)->result();
          $q['data_count'] = $this->M_login->get_edit($post)->num_rows();

          $this->load->view('operator/profile', $q);

        } elseif ($this->session->userdata('akses') == '3') {
          
          $q['data_level'] = $this->M_login->get_level()->result();
          $q['data'] = $this->M_login->get_edit($post)->result();
          $q['data_count'] = $this->M_login->get_edit($post)->num_rows();

          $this->load->view('peminjam/profile', $q);

        } 
      }
    }
  }

  function sunting_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['id_petugas']);

    if (!isset($post['check'])) {
      foreach ($post['id_petugas'] as $key => $val) {
        $result[] = array(
          "id_petugas" => $post['id_petugas'][$key],
          "nama_petugas" => $post['nama_petugas'][$key],
          "username" => $post['username'][$key],
          "id_level" => $post['id_level'][$key]
        );
      }
    } else {
      foreach ($post['id_petugas'] as $key => $val) {
        $result[] = array(
          "id_petugas" => $post['id_petugas'][$key],
          "nama_petugas" => $post['nama_petugas'][$key],
          "username" => $post['username'][$key],
          "password" => sha1($post['password'][$key]),
          "id_level" => $post['id_level'][$key]
        );
      }
    }
    $this->M_login->post_edit($result);

    if ($result) {
      echo "<script> alert('Profile berhasil di perbarui, Silahkan login kembali'); window.location = '".base_url('Login/logout')."'; </script>";
    }
  }

}