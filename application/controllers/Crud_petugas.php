<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_petugas extends CI_Controller {

  function __construct() {
    parent::__construct();
    //validasi jika user belum login
    if ($this->session->userdata('masuk') != TRUE) {
      $url = base_url();
      redirect($url);
    }
    $this->load->model('Crud_petugas_m');
  }

  function index() {

    if ($this->session->userdata('akses') == '1') {
      
      $q['data'] = $this->Crud_petugas_m->get_list()->result();
      $q['data_count'] = $this->Crud_petugas_m->get_list()->num_rows();
      $this->load->view('admin/v_petugas', $q);
      
    } elseif ($this->session->userdata('akses') == '2') {
      $this->load->view('error404');
    } elseif ($this->session->userdata('akses') == '3') {
      $this->load->view('error404');
    }
    
  }

  function tambah() {
    $get = $this->input->get();
    $q['data_level'] = $this->Crud_petugas_m->get_level()->result();
    $q['total_form'] = $get['total_form'];
    $this->load->view('admin/v_petugas_tambah', $q);
  }

  function tambah_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['nama_petugas']);

    foreach ($post['nama_petugas'] as $key => $val) {

      if (empty($post['id_level'])) {
        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data Level tidak boleh kosong!</div>');
        redirect('Crud_petugas/tambah?total_form=1&submit=ok');
      } else {
        $result[] = array(
          "nama_petugas" => $post['nama_petugas'][$key],
          "username" => $post['username'][$key],
          "password" => sha1($post['password'][$key]),
          "id_level" => $post['id_level'][$key]
        );
      }
    }
    $this->Crud_petugas_m->post_add($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di tambahkan!</div>');
    redirect('Crud_petugas');
  }

  function sunting_hapus() {
    $post = $this->input->post();
    $check = $post['check'];

    if (isset($check)) {
      if (isset($post['sunting'])) {
        $q['data_level'] = $this->Crud_petugas_m->get_level()->result();
        $q['data'] = $this->Crud_petugas_m->get_edit($post)->result();
        $q['data_count'] = $this->Crud_petugas_m->get_edit($post)->num_rows();

        $this->load->view('admin/v_petugas_edit', $q);
      } elseif (isset($post['hapus'])) {
        $this->Crud_petugas_m->post_delete($post);

        $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
          .count($check).' data berhasil di hapus!</div>');
        redirect('Crud_petugas');
      }
    } else {
      $this->session->set_flashdata('notif', '<div class="alert alert-primary" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Harap centang dulu datanya!</div>');
      redirect('Crud_petugas');
    }
  }

  function sunting_proses() {
    $post = $this->input->post();
    $result = array();
    $total_post = count($post['id_petugas']);

    foreach ($post['id_petugas'] as $key => $val) {
      $result[] = array(
        "id_petugas" => $post['id_petugas'][$key],
        "nama_petugas" => $post['nama_petugas'][$key],
        "username" => $post['username'][$key],
        "id_level" => $post['id_level'][$key]
      );
    }
    $this->Crud_petugas_m->post_edit($result);

    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
      .$total_post.' data berhasil di perbarui!</div>');
    redirect('Crud_petugas');
  }

}