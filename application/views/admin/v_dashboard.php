<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Renime">

  <title>CInventoria | Dashboard</title>

  <!-- Load Link CSS,Favicon,Google Font -->
  <?php $this->load->view('admin/link-css-header'); ?>
  
</head>
<body>

<div class="wrapper">

  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3 class="text-center"><i class="fa fa-box-open"></i>CInventoria~</h3>
    </div>

    <ul class="list-unstyled components">      
      <li class="active">
        <a href="<?php echo base_url().'Page/' ?>"><i class="fa fa-tachometer-alt mr-2"></i> Dashboard</a>
      </li>
      <li>
        <a href="#userdrop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-users mr-2"></i> User
        </a>
        <ul class="collapse list-unstyled" id="userdrop">
          <li><a href="<?php echo base_url().'Page/data_petugas' ?>" class="pl-5">Admin & Operator</a></li>
          <li><a href="<?php echo base_url().'Page/data_pegawai' ?>" class="pl-5">Pegawai / Guru</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_ruang' ?>"><i class="fa fa-home mr-2"></i> Tempat / Ruangan</a>
      </li>
      <p class="text-center pe pb-3">Manajemen Barang</p>
      <li>
        <a href="<?php echo base_url().'Page/jenis_barang' ?>"><i class="fa fa-tags mr-1"></i> Jenis Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_barang' ?>"><i class="fa fa-box-open mr-1"></i> Barang</a>
      </li>
      <li>
        <a href="<?php echo base_url().'Page/data_peminjaman' ?>"><i class="fa fa-laptop mr-1"></i> Barang Pinjam</a>
      </li>
      <li>
        <a href="#userdrop1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fa fa-file-pdf mr-2"></i> Laporan
        </a>
        <ul class="collapse list-unstyled" id="userdrop1">
          <li><a href="<?php echo base_url().'Page/laporan' ?>" class="pl-5">Barang</a></li>
          <li><a href="<?php echo base_url().'Page/laporan_pinjam' ?>" class="pl-5">Peminjaman</a></li>
        </ul>
      </li>
    </ul>

    <!-- Load file sidebar-foot.php -->
    <?php $this->load->view('admin/sidebar-foot'); ?>
  </nav>

  <!-- Content  -->
  <div id="content">

    <!-- Load file navbar.php -->
    <?php $this->load->view('admin/navbar') ?>

    <?=$this->session->flashdata('notif');?>

    <div class="jumbotron text-center bg-white p-4">
      <h1 class="display-6">Selamat Datang di CInventoria!<h1>
      <p class="lead">--- anda telah login sebagai admin ---</p>
    </div>

    <div class='row'>

      <div class="dashboard-cards text-center pb-3">
        <a href="<?php echo base_url().'Page/data_petugas' ?>">
          <div class='card col-md-4'>
            <div class='card-title'>
              <h2>
                Total Admin
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Crud_petugas_m->get_jml_admin(); ?>
              </div>
            </div>
          </div>
        </a>

        <a href="<?php echo base_url().'Page/data_petugas' ?>">
          <div class='card col-md-3'>
            <div class='card-title'>
              <h2>
                Total Operator
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Crud_petugas_m->get_jml_operator(); ?>
              </div>
            </div>
          </div>
        </a>

        <a href="<?php echo base_url().'Page/data_pegawai' ?>">
          <div class='card col-md-4'>
            <div class='card-title'>
              <h2>
                Total Pegawai
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Crud_pegawai_m->get_jml_peminjam(); ?>
              </div>
            </div>
          </div>
        </a>

        <a href="<?php echo base_url().'Page/jenis_barang' ?>">
          <div class='card col-md-4'>
            <div class='card-title'>
              <h2>
                Total Jenis Barang
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Crud_jenis_barang_m->get_jml_jenis(); ?>
              </div>
            </div>
          </div>
        </a>

        <a href="<?php echo base_url().'Page/data_barang' ?>">
          <div class='card col-md-3'>
            <div class='card-title'>
              <h2>
                Total Barang
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Crud_barang_m->get_jml_barang(); ?>
              </div>
            </div>
          </div>
        </a>

        <a href="<?php echo base_url().'Page/' ?>">
          <div class='card col-md-4'>
            <div class='card-title'>
              <h2>
                Total Barang Pinjam
                <small>Klik untuk info detail</small>
              </h2>
              <div class='task-count'>
                <?php echo $this->Data_peminjaman_m->get_jml_pinjam(); ?>
              </div>
            </div>
          </div>
        </a>
      </div>

    </div>

    <div class="card">
      <div class="card-header">
        Data Barang
      </div>
      <div class="card-body">
        <div class="table-responsive pt-2">
          <table class="table table-bordered" id="example1">
            <thead class="bg-core">
              <tr class="text-center">
                <th width="10">No</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Spesifikasi</th>
                <th>Kondisi</th>
                <th>Jumlah</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Keterangan</th>
                <th>Petugas</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                if ($data_count == 0) {
                  echo "data kosong";
                } else {
                  $no = 1;
                  foreach ($data as $v) {
                    echo "<tr>";
                    echo "<td class='text-center'>".$no++."</td>";
                    echo "<td>".$v->kode_inventaris."</td>";
                    echo "<td>".$v->nama."</td>";
                    echo "<td>".$v->spesifikasi."</td>";
                    echo "<td>".$v->kondisi."</td>";
                    echo "<td>".$v->jumlah."</td>";
                    echo "<td>".$v->nama_jenis."</td>";
                    echo "<td>".$v->nama_ruang."</td>";
                    if ($v->ket == 'Y') {
                      echo "<td><span class='badge badge-primary'>Tersedia</span></td>";
                    } else {
                      echo "<td><span class='badge badge-warning'>Tidak Tersedia</span></td>";
                    }                         
                    echo "<td>".$v->nama_petugas."</td>";
                    echo "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
</div>

  <!-- Load link js -->
  <?php $this->load->view('admin/link-js-footer'); ?>

</body>
</html>