    <nav class="navbar navbar-expand-lg navbar-light bg-white mb-4" id="myHeader">
      <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-core">
          <i class="fas fa-align-left"></i>
          <span>Toggle Sidebar</span>
        </button>

        <div class="navbar-brand d-lg-none">
          <a href="" class="text-core"><i class="fa fa-box-open"></i> CInventoria</a>
        </div>

        <button class="btn btn-core d-inline-block d-lg-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false">
          <i class="fas fa-align-right"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link">Hello, <?php echo $this->session->userdata('ses_name'); ?></a>
            </li>
          </ul>
        </div>

      </div>
      <div class="navbarp ignielPelangi mt-0 mb-3"></div>
    </nav>
    