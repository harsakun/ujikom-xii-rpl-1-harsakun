<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_peminjaman_m extends CI_Model {
  
  function viewByNis($kode){
    $this->db->where('kode_inventaris', $kode);
    $result = $this->db->get('inventaris')->row(); // Tampilkan data siswa berdasarkan NIS
    
    return $result; 
  }

  function get_jml_pinjam() {
    return $this->db->query("SELECT * FROM peminjaman")->num_rows();
  }

  function get_list() {
    return $this->db->query("SELECT * FROM peminjaman p INNER JOIN detail_pinjam d ON p.id_detail_pinjam=d.id_detail_pinjam JOIN inventaris i ON d.id_inventaris=i.id_inventaris JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN petugas t ON d.id_petugas=t.id_petugas ORDER BY id_peminjaman DESC");
  }

  function getDetail() {
    return $this->db->query("SELECT * FROM detail_pinjam ORDER BY id_detail_pinjam DESC");    
  }

  function create($table,$data){
    $query = $this->db->insert($table, $data);
    return $this->db->insert_id();// return last insert id
  }

  function update_data($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  } 

}